# powerlaw

Power-Law Distribution Analysis based on [Power-law distributions in Empirical data](http://arxiv.org/pdf/0706.1062.pdf) paper

### Introduction

A variable x is said to obey a power law if it is drawn from a probability distribution of the form *p(x) = Cx<sup>-&alpha;</sup>* where *C* is called the normalization constant and *&alpha;* is called scaling parameter or exponent. Often, the power law applies only for values greater than some minimum *x*, called *x<sub>min</sub>*. The paper describes various statistical techniques to test if a given distribution follows a power law or not.
